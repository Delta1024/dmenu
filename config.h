static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int centered = 0;                    /* -c option; centers dmenu on screen */
static int min_width = 500;                    /* minimum width when centered */

static const char *fonts[] = {
	"FiraCode Nerd Font:size=12"
};

static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bbbbbb", "#222222" },
	[SchemeSel] = { "#eeeeee", "#228B22" },
	[SchemeOut] = { "#000000", "#00ffff" },
};

static unsigned int lines      = 0;

static const char worddelimiters[] = " ";
